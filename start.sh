#!/bin/bash
if [ $# != 1 ]; then
    echo "usage: ./start.sh <file>"
elif [[ $EUID -ne 0 ]]; then
    echo "This script must run as root"
else
    make clean
    make all
    iptables -F
    iptables -A OUTPUT -j NFQUEUE --queue-num 0
    iptables -A INPUT -j NFQUEUE --queue-num 0
    ./1m_block $1
fi