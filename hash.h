#include <stdio.h>
#include <math.h>
#include <iostream>
#include <openssl/sha.h>

using namespace std;

struct data  {
    u_int64_t key;
    string *value;
};

    

struct hashtable_item  {
    int flag = 0;
    /*

        * flag = 0 : data not present

        * flag = 1 : some data already present

        * flag = 2 : data was present,but deleted

    */
    struct data *item;
};

    
class HashTable {
    public:
        int insert(u_int64_t key, string *value);
        string search(u_int64_t key);
        /* displays all elements of array */

        void display();

        /* initializes array */

        HashTable(int max);
        ~HashTable();        
            
    private:
        int max;
        int table_size;
        int prime;
        struct hashtable_item *array;

        int hashcode1(u_int64_t key);

        int hashcode2(u_int64_t key);
        /* returns largest prime number less than size of array */

        int get_prime();
        int size_of_hashtable();
};
u_int64_t get_key_from_string(string value);
