#include <stdio.h>
#include <math.h>
#include <iostream>
#include "hash.h"
#include <openssl/sha.h>
using namespace std;

HashTable :: HashTable(int key_num) {
    max = key_num * 3;
    printf("max: %d\n", max);
    table_size = 0;
    printf("size: %d\n", table_size);
    prime = get_prime();
    printf("prime: %d\n", prime);
    array = (struct hashtable_item*) malloc(max * sizeof(struct hashtable_item));
}
HashTable :: ~HashTable() {
    int i;
    for (i = 0; i < max; i++) {
        if (array[i].flag == 1) {
            free(array[i].item);
        }
    }
    free(array);
}
int HashTable :: insert(u_int64_t key, string *value){
    if (table_size == max){
        printf("\n Hash Table is full, cannot insert more items \n");
        return -1;
    }

    if ((*value).empty()) {
            printf("\n Value should not be empty\n");
        return -1;
    }
    int hash1 = hashcode1(key);
    int hash2 = hashcode2(key);

    int index = hash1;
    /* create new data to insert */
    struct data *new_item = (struct data*) malloc(sizeof(struct data));

    new_item->key = key;
    new_item->value = value;

    /* probing through other array elements */
    while (array[index].flag == 1) {
        if (array[index].item->key == key){
            cout << "same key";
            array[index].item->value = new string((*array[index].item->value) + "&" + (*value));
            cout << *array[index].item->value;
            free(new_item);
            return 0;
        }

        index = (index + hash2) % max; 
        if (index == hash1){
            cout << "\n Add is failed \n";
            free(new_item);
            return -1;
        }
    }

    array[index].item = new_item;
    array[index].flag = 1;
    table_size++;
    return 0;
}

string HashTable :: search(u_int64_t key) {
    int hash1 = hashcode1(key);
    int hash2 = hashcode2(key);

    int index = hash1;
    while (array[index].flag == 1) {
        if (array[index].item->key == key) {
            return (*array[index].item->value);
        }
        index = (index + hash2) % max;
        if (index == hash1){
            return "";
        }
    }
    return "";
}

/* displays all elements of array */

void HashTable :: display() {
    int i;
    for (i = 0; i < max; i++) {
        if (array[i].flag == 1) {
            cout << "\n Array[" << i <<  "] has elements \n Key (" << array[i].item->key << ") and Value (" + (*array[i].item->value) + ") \n";
        }
    }
}


int HashTable :: hashcode1(u_int64_t key) {
    return (key % max);
}

int HashTable :: hashcode2(u_int64_t key) {
    return (prime - (key % prime));
}

int HashTable :: get_prime() {
    int i,j;
    for (i = max - 1; i >= 1; i--) {

        int flag = 0;
        for (j = 2; j <= (int)sqrt(i); j++) {
            if (i % j == 0) {
                flag++;
            }
        }
        if (flag == 0) {
            return i;
        }

    }
    return 3;
}
int HashTable :: size_of_hashtable() {
    return table_size;
}

u_int64_t get_key_from_string(string value){
    if (value.empty()) {
            cout << "\n Value should not be empty\n";
        return -1;
    }
    unsigned char key_string[20];
    const unsigned char* cstr = (unsigned char*)value.c_str();
    u_int64_t key = 0;
    SHA1(cstr, value.length(), key_string);
    key_string[8] = '\0';
    for (int i = 0; i < 8 ; i++) {
        key = (key << 8) +(u_int64_t)key_string[i];
    }

    return key;
}