all : 1m_block

1m_block: main.o search.o hash.o
	g++ -g -o 1m_block main.o search.o hash.o -lssl -lcrypto -lnetfilter_queue
search.o:
	g++ -g -c -o search.o search.cpp
hash.o:
	g++ -g -c  -o hash.o hash.cpp -lssl -lcrypto
main.o:
	g++ -g -c -o main.o main.cpp
clean:
	rm -f 1m_block
	rm -f *.o
