#!/bin/bash
if [[ $EUID -ne 0 ]]; then
    echo "This script must run as root"
else
    make clean;
    iptables -F
fi